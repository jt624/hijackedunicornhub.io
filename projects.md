
### Markdown

Markdown is a lightweight and easy-to-use syntax for styling your writing. It includes conventions for

```markdown
Syntax highlighted code block

# Jennifer Tabor
## CS 499
####### Southern New Hampshire University

- **Milestone One**
- [] Upload Milestone One
[Code Review]("C:\Users\user\Desktop\codeReview.zip")
- **Milestone Two**
- [] Upload Milestone Two
- **Milestone Three**
- [] Upload Milestone Three
- **Milestone Four**
- [x] Upload Milestone Four
[Milestone Four Narrative](C:\Users\user\Desktop\CS 499_Milestone Four_Jennifer Tabor.docx)
[Android Project Updated](C:\Users\user\Desktop\CS 499\PortfolioNotes_MilestoneFour_JenniferTabor)


**Bold** and _Italic_ and `Code` text

[Link](url) and ![Image](src)
```

For more details see [GitHub Flavored Markdown](https://guides.github.com/features/mastering-markdown/).

### Jekyll Themes

Your Pages site will use the layout and styles from the Jekyll theme you have selected in your [repository settings](https://github.com/hijackedUnicorn/hijackedUnicorn.github.io/settings). The name of this theme is saved in the Jekyll `_config.yml` configuration file.

### Support or Contact

Having trouble with Pages? Check out our [documentation](https://help.github.com/categories/github-pages-basics/) or [contact support](https://github.com/contact) and we’ll help you sort it out.
